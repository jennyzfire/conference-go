import requests

OPEN_WEATHER_API_KEY = "28a958ed9459f5e853c1dbbcce1901b0"


city = "Philadelphia"
state = "PA"

url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"

res = requests.get(url)
print(res.json())

lat = res.json()[0]['lat']
lon = res.json()[0]['lon']

print("lat:", lat)
print("lon:", lon)


url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"


res = requests.get(url)
weather_description = res.json()['weather'][0]['description']
print(weather_description)
current_temperature = res.json()['main']['temp']
print(current_temperature)
