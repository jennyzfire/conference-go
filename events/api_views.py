from django.http import JsonResponse
from .models import Conference, Location, State
from common.modelEncoder import ConferenceListEncoder, ConferenceDetailEncoder
from common.modelEncoder import LocationListEncoder, LocationDetailEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_image, get_coordinates, get_weather


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
    else:  # POST
        content = json.loads(request.body)
        # in the dictionary of content, find the key called "location"
        # and return its value
        # because to create a conference you need to first get the location
        # which is a foreign key
        location = Location.objects.get(id=content["location"])
        content["location"] = location
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    return JsonResponse(conferences, encoder=ConferenceListEncoder, safe=False)  # it can parse
    #  more than just dictionaries


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, pk):
    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        coords = get_coordinates(conference.location.city, conference.location.state.abbreviation)
        weather = get_weather(coords["lat"], coords["lon"])
        return JsonResponse(
            {"conference": conference, "weather": weather}, encoder=ConferenceDetailEncoder, safe=False
        )

    elif request.method == "PUT":
        content = json.loads(request.body)
        Conference.objects.filter(id=pk).update(**content)

        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )

    else:
        conference = Conference.objects.get(pk=pk)
        count, _ = Conference.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})

    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
    # results = []
    # for location in locations:
    #     results.append({"name": location.name, "href": location.get_api_url()})
    else:  # POST, create a location
        content = json.loads(request.body)  # because will be POST, request.body is user data entered

        content["image_url"] = get_image(content['city'], content['state'])

        # get the city and state from content
        # make a request to pexel for an image
        # get first image from pexel request
        # add image to new model
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            locations = Location.objects.create(**content)
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
                safe=False
            )
    return JsonResponse(locations, encoder=LocationListEncoder, safe=False)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        location = Location.objects.get(pk=pk)
        location.image_url = get_image(location.city, location.state)
        coords = get_coordinates(location.city, location.state.abbreviation)
        print(coords)
        weather = get_weather(coords["lat"], coords["lon"])
        print(weather)
        return JsonResponse(
            {"location": location}, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # METHOD IS PUT/PATCH
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.get(pk=pk)
        Location.objects.filter(pk=pk).update(**content)

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
