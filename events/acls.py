from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_image(city, state):
    headers = {
        'Authorization': PEXEL_API_KEY
    }
    res = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1", headers=headers)

    return res.json()['photos'][0]['src']['original']


def get_coordinates(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"

    res = requests.get(url)
    print(res.json())

    lat = res.json()[0]['lat']
    lon = res.json()[0]['lon']

    return {
        "lat": lat,
        "lon": lon,
    }
# response[0] <-- array index
# response[0]['local_names']
# response[0]['local_name']['en']


def get_weather(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

    res = requests.get(url)
    weather_description = res.json()['weather'][0]['description']
    print(weather_description)
    current_temperature = res.json()['main']['temp']
    print(current_temperature)
    return {
        "weather_description": weather_description,
        "current_temperature": current_temperature
    }
