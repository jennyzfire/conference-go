def list_to_dict(lst):
    d = {}
    key = None
    value = None
    for element in lst:
        if lst.index(element) % 2 != 0:
            key = element
        if lst.index(element) % 2 == 0:
            value = element
        d[key] = value
    return d



print(list_to_dict(["a", 1, "b", 2]))
# --> {"a": 1, "b": 2}
